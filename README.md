# Resize S3 Images w AWS Lambda 

Node JS code used as AWS Lambda to resize images placed on AWS S3 bucket

## Local Testing
```
npm install
```

## Uploading to AWS Lambda
- Configure AWS Lambda to use similar nodejs version
- Install locally (lsee local testing above)
- Zip and upload zipped file to AWS Lambda

## Other Configurations
- Create IAM for Lamda role with permissions to lambda, and cloudwatch
- Create S3 bucket
- Create Lambda, assign permission, add trigger for S3 and upload the nodejs zipfile.