"use strict";

const Sharp = require("sharp");
const AWS = require("aws-sdk");
const S3 = new AWS.S3();

exports.handler = async function (event, context, callback) {
  console.log("Incoming Event: ", event);
  const bucket = event.Records[0].s3.bucket.name;
  const filename = decodeURIComponent(
    event.Records[0].s3.object.key.replace(/\+/g, " ")
  );
  const message = `File is uploaded in - ${bucket} -> ${filename}`;
  console.log(message);

  if (event.Records[0].eventName === "ObjectRemoved:Delete") {
    return;
  }

  try {
    console.log("getting image object from S3...");
    let image = await S3.getObject({ Bucket: bucket, Key: filename }).promise();
    console.log("received image object from S3. Using Sharp to get image");
    image = await Sharp(image.Body);
    console.log("Image received from Sharp. Reading metaData...");
    const metaData = await image.metadata();
    console.log(metaData);

    if (metaData.width > 500) {
      console.log(`resizing image from ${metaData.width} to 500`);
      const resizedImage = await image.resize({ width: 500 }).toBuffer();
      console.log("image successfully resized. placing back on S3");
      await S3.putObject({
        Bucket: bucket,
        Body: resizedImage,
        Key: filename,
      }).promise();
      return;
    } else {
      return;
    }
  } catch (error) {
    context.fail(`Error resizing image. ${error}`);
  }

  callback(null, message);
};
